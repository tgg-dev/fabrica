---
layout: post
title:  "Sin Oxigeno"
date:   2024-11-01 18:30:23 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Sientes lo incompleto.</p>
<p class="text-center">Mientra fumas cigarros.</p>
<p class="text-center">Tus ojos son decepción.</p>
<p class="text-center">Tan patético este amor.</p>
<p class="text-center">Mirando esos feos ojos.</p>
<p class="text-center">Tu hombre era un niño.</p>
<br>
<p class="text-center">Hoy estoy sin oxigeno.</p>
<p class="text-center">Para odiarte.</p>
<br>
<p class="text-center">Hoy estoy sin oxigeno.</p>
<p class="text-center">Para amarte.</p>
<br>
<p class="text-center">El otro lado es igual.</p>
<p class="text-center">La amargura ya no esta.</p>
<p class="text-center">Las luces se apagaran.</p>
<p class="text-center">Como lo que pasara acá.</p>
<p class="text-center">Todo te sabe a cenizas.</p>
<p class="text-center">Y todo se vuelve niebla.</p>
<br>
<p class="text-center">Hoy estoy sin oxigeno.</p>
<p class="text-center">Para ahogarme.</p>
<br>
<p class="text-center">Hoy estoy sin oxigeno.</p>
<p class="text-center">Para colgarme.</p>
<br>
<p class="text-center">En el auto ya es igual.</p>
<p class="text-center">Da lo mismo ya volcar.</p>
<p class="text-center">Y solo quieres gritar.</p>
<p class="text-center">Mientras el se apago.</p>
<p class="text-center">Como hielo el trato.</p>
<p class="text-center">Afuera quema el sol.</p>
<p class="text-center">Hoy estoy sin oxigeno.</p>
<p class="text-center">Para poder.</p>
<br>
<p class="text-center">Hoy estoy sin oxigeno.</p>
<p class="text-center">Para perder.</p>
<br>
<p class="text-center">Hoy estoy sin oxigeno.</p>
<p class="text-center">Para vencer..</p>