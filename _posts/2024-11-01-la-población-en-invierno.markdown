---
layout: post
title:  "La población en invierno"
date:   2024-11-01 18:30:29 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Jugar a perseguido cuando suenan zapatillas.</p>
<p class="text-center">Cabeceando mientras estas ideas te asaltan.</p> 
<p class="text-center">El quiere más dinero por volverse un burro.</p>
<p class="text-center">Y pensar en libertad creyendo que la obtuvo.</p>
<br>
<p class="text-center">La población siempre feo.</p>
<p class="text-center">La población en invierno.</p>
<br>
<p class="text-center">Banderas de los colores que más te gustan.</p>
<p class="text-center">No hablar de eso porque en la mesa pelean.</p>
<p class="text-center">Calles mojadas que huele a olvido y perro.</p>
<p class="text-center">Huele a trago, meado, basuras y plásticos.</p>
<br>
<p class="text-center">La población es basurero.</p>
<p class="text-center">La población en invierno.</p>
<br>
<p class="text-center">Las casas ya serán esas grandes habitaciones.</p> 
<p class="text-center">En las fiestas se hablara como pasó el lunes.</p>
<p class="text-center">Mientras el alcohol duerme nuestros recuerdos.</p>
<p class="text-center">Y al fondo se escucha el sueldo no me alcanzo.</p>
<br>
<p class="text-center">La población serás lento.</p>
<p class="text-center">La población en invierno.</p>
<br>
<p class="text-center">Para ti cercano a tener las vacaciones.</p>
<p class="text-center">Es ver esas gotas cayendo de las nubes.</p>
<p class="text-center">Observando esos hermosos días lluviosos.</p>
<p class="text-center">Olor a asfalto mojado ya se hizo un lago.</p>
<br>
<p class="text-center">La población en proceso.</p>
<p class="text-center">La población en invierno.</p>
<br>
<p class="text-center">Desde chico, saber tomar buen alcohol.</p>
<p class="text-center">Fumando el cigarro que mata de a poco.</p> 
<p class="text-center">Una manera hermosa de perder el tiempo.</p>
<p class="text-center">Saquemos toda mascara eso siempre paso.</p>
<br>
<p class="text-center">La población en cuaderno.</p>
<p class="text-center">La población en invierno.</p>