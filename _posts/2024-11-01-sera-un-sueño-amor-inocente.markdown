---
layout: post
title:  "¿Será un sueño? (Amor inocente)"
date:   2024-11-01 18:30:41 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">El sol viene, tu junto a mi.</p>
<p class="text-center">Y me hablas, me pierdo en ti.</p>
<p class="text-center">Me diste un beso y no volví.</p>
<p class="text-center">Decir te amo, yo ya lo vivi.</p>
<br>
<p class="text-center">Amanece el amor.</p>
<p class="text-center">Siento latir mi corazón.</p>
<p class="text-center">El sol a lo lejos.</p>
<p class="text-center">Pero mi vida, ya amaneció.</p>
<br>
<p class="text-center">Te sonrojas, no se que decir.</p>
<p class="text-center">Aun siento el sabor de vivir.</p>
<p class="text-center">Y otras vez, eso vuelve a ti.</p>
<p class="text-center">Mil frases por segundo en mi.</p>
<br>
<p class="text-center">Amanece el amor.</p>
<p class="text-center">Siento latir mi corazón.</p>
<p class="text-center">El sol a lo lejos.</p>
<p class="text-center">Pero mi vida.</p> 
<p class="text-center">hoy amanece para todos.</p>
<br>
<p class="text-center">No se besar, te digo a ti.</p>
<p class="text-center">Aun cuanto me lo decía a mi.</p>
<p class="text-center">Ahora siento que no hay fin.</p>
<p class="text-center">Por un segundo todo se paró.</p>
<br>
<p class="text-center">¿Será esto un sueño?...</p>
<br>
<p class="text-center">Amanece el amor.</p>
<p class="text-center">Siento latir mi corazón.</p>
<p class="text-center">El sol a lo lejos.</p>
<p class="text-center">Pero mi vida.</p> 
<p class="text-center">hoy amanece para todos.</p>