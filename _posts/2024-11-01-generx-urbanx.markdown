---
layout: post
title:  "Generx Urbanx"
date:   2024-11-01 18:30:17 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">No entiendo yo tu arte.</p>
<p class="text-center">Urbanos que no eran.</p>
<p class="text-center">Críticas a traficantes.</p>
<p class="text-center">Pero eres clientela.</p>
<p class="text-center">Comentan las balaceras.</p>
<p class="text-center">Pero no esas balas.</p>
<br>
<p class="text-center">Caigo en la verdad.</p>
<p class="text-center">No soy un simplón.</p>
<br>
<p class="text-center">Ritmo como coctelera.</p>
<p class="text-center">Urbana de casa.</p>
<p class="text-center">Repite esa cantaleta.</p>
<p class="text-center">Llenando un bar.</p>
<p class="text-center">Jockey en la cabeza.</p>
<p class="text-center">Tapa esa pelada.</p>
<br>
<p class="text-center">Caigo en la verdad.</p>
<p class="text-center">No soy un simplón.</p>
<br>
<p class="text-center">Es el no ves, no lo reconoces.</p>
<p class="text-center">Su buzo deportivo,</p> 
<p class="text-center">Gafas de sol,</p>
<p class="text-center">joyas de oro</p>
<br>
<p class="text-center">No soy un simplón.</p> 
<p class="text-center">No soy un simplón.</p>
<p class="text-center">No soy un simplón.</p>
<br>
<p class="text-center">Erradicar la pobreza.</p>
<p class="text-center">Les dispararías.</p>
<p class="text-center">Esas gafas son oscuras.</p>
<p class="text-center">Verdad escondida.</p>
<p class="text-center">El pobre ve esa proeza.</p>
<p class="text-center">Y le hacen oral.</p>
<br>
<p class="text-center">Caigo en la verdad.</p>
<p class="text-center">No soy un simplón.</p>
<br>
<p class="text-center">El es no ves el era de aquí.</p>
<p class="text-center">Llega en su deportivo,</p> 
<p class="text-center">Con una mujer,</p> 
<p class="text-center">De plástico.</p>
<br>
<p class="text-center">No soy un simplón.</p> 
<p class="text-center">No soy un simplón.</p>
<br>
<p class="text-center">Caigo en la verdad.</p>
<p class="text-center">No soy un simplón.</p>
<br>
<p class="text-center">Caigo en la verdad.</p>
<p class="text-center">No soy un simplón.</p>
<br>
<p class="text-center">Me caga tu verdad.</p>
<p class="text-center">No soy un simplón.</p>