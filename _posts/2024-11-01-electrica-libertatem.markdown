---
layout: post
title:  "Eléctrica libertatem"
date:   2024-11-01 18:30:18 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Enciende esa radio y asocias música.</p>
<p class="text-center">Eres libre del día a día otra vez.</p>
<p class="text-center">Sientes que esa misma es tu historia.</p>
<p class="text-center">Pero el que canta no te conoce a ti.</p>

<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, comerciales.</p>

<p class="text-center">Hagamos fiestas después de las doce.</p>
<p class="text-center">Abran puertas de empresas otra vez.</p> 
<p class="text-center">La farmacia no entrega hoy condones.</p>
<p class="text-center">Mañana el hijo de doce será padre.</p>

<p class="text-center">Libertad electrica, otra vez.</p> 
<p class="text-center">Libertad electrica, otra vez.</p> 
<p class="text-center">Libertad electrica, on lain agein.</p>

<p class="text-center">Y con la radio.</p>
<p class="text-center">Libre otra vez.</p>
<p class="text-center">Y computadores.</p>
<p class="text-center">Libre otra vez.</p>
<p class="text-center">Con la canasta.</p>
<p class="text-center">Libre otra vez.</p>
<p class="text-center">Estar en cana.</p>
<p class="text-center">Libre otra vez.</p>

<p class="text-center">Su madre la vio igual a un huevo.</p>
<p class="text-center">La fantasía en ese lugar se quebró.</p> 
<p class="text-center">Su hermana ve la sangre y el cuerpo.</p>
<p class="text-center">Los perdigones no solo matan conejos.</p>

<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, otra vez .</p>
<p class="text-center">Libertad eléctrica, sin sangre.</p>

<p class="text-center">Tu arriba lo ves y no se entendió.</p>
<p class="text-center">Esa cultura rara que ya se enfrió.</p>
<p class="text-center">No estás celoso, no quieres verlo.</p>
<p class="text-center">Ese olimpo tan extraño solo viajo.</p>

<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, ojos azulez.</p>

<p class="text-center">Y las plumas.</p>
<p class="text-center">Libre otra vez.</p>
<p class="text-center">Y prostitutas.</p>
<p class="text-center">Libre otra vez.</p>
<p class="text-center">La alta gama.</p>
<p class="text-center">Libre otra vez.</p>
<p class="text-center">Venir de Francia.</p>
<p class="text-center">Libre otra vez.</p>


<p class="text-center">Ya sus alas no son muy grandes.</p>
<p class="text-center">Y los deseos de vivir en Nantes.</p>
<p class="text-center">Su patria son esas inversiones.</p>
<p class="text-center">Sus hijos de nana y diversiones.</p>

<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, pasaporte.</p>

<p class="text-center">La indecencia no tocaría lo blanco.</p>
<p class="text-center">Si mi nana lo lava harto no lo vio.</p>
<p class="text-center">La población tiene hijos afortunados.</p> 
<p class="text-center">Ser futbolista te vuelve de rancio.</p>

<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, otra vez.</p> 
<p class="text-center">Libertad eléctrica, triunfaste.</p>

<p class="text-center">Enciende esa radio y asocias música.</p>