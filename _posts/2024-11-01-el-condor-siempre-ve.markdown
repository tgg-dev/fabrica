---
layout: post
title:  "El cóndor siempre ve"
date:   2024-11-01 18:30:48 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">En la cumbre pude ver y conocer.</p>
<p class="text-center">Lo pequeño que el hombre es.</p>
<br>
<p class="text-center">Volando en lo alto.</p> 
<p class="text-center">En ingravidez.</p>
<p class="text-center">El cóndor todo lo ve.</p>
<p class="text-center">El cóndor lo ve.</p>
<br>
<p class="text-center">Subir cerros pude ver y conocer.</p>
<p class="text-center">Lo enorme que la montaña es.</p>
<br>
<p class="text-center">Volando en lo alto.</p>
<p class="text-center">En ingravidez.</p>
<p class="text-center">El cóndor todo lo ve.</p>
<p class="text-center">El cóndor lo ve.</p>
<br>
<p class="text-center">Subir cerros pude ver y conocer.</p>
<p class="text-center">Lo enorme que la galaxia es.</p>
<br>
<p class="text-center">Volando en lo alto.</p> 
<p class="text-center">En ingravidez.</p>
<p class="text-center">El cóndor todo lo ve.</p>
<p class="text-center">El cóndor lo ve.</p>
<p class="text-center">El cóndor siempre ve.</p>