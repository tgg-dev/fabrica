---
layout: post
title:  "Intrexto"
date:   2024-11-01 18:30:29 -0300
categories: poesia
featured_image: https://i.imgur.com/TNXeA5v.jpeg
---
<p class="text-center">Todo dia es historia.</p>
<p class="text-center">Y nacen unas Lúdicas.</p>

<p class="text-center">Luces rojas de calle.</p>
<p class="text-center">No hay mucho aguante.</p>

<p class="text-center">Acá viene Intrexto.</p>
<p class="text-center">Equilibrio Cósmico.</p>
<p class="text-center">Con Drama Escénico.</p>

<p class="text-center">Micros vienen llenas.</p>
<p class="text-center">Todas van a Metropia.</p>

<p class="text-center">Idolxs bien ausentes.</p>
<p class="text-center">Disfrazados doctores.</p>

<p class="text-center">Aca viene Intrexto.</p>
<p class="text-center">Tiñe alivios cómicos.</p>
<p class="text-center">Mira tu cara espejo.</p>

<p class="text-center">Electrica Libertatem.</p>
<p class="text-center">Amores ya comerciales.</p>

<p class="text-center">Luisa no tiene pareja.</p>
<p class="text-center">Compro nueva lavadora.</p>

<p class="text-center">Aca viene Intrexto.</p>
<p class="text-center">La pobla en invierno.</p>
<p class="text-center">Todos anestesiados….</p>