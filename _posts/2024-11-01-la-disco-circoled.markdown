---
layout: post
title:  "La disco (Circoled)"
date:   2024-11-01 18:30:30 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Su cuerpo al sonido, baila.</p>
<p class="text-center">Y su vida es sonido, baila.</p>
<br>
<p class="text-center">Se mueve.</p>
<p class="text-center">Se mueve el Circoled.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Se mueve.</p>
<p class="text-center">Te pudre.</p>
<p class="text-center">Te mueres.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Afuera matan gente y desaparecen.</p>
<p class="text-center">Olvido esa gente.</p>
<p class="text-center">Con luz uv todo será brillante.</p>
<p class="text-center">Olvido esa gente.</p>
<br>
<p class="text-center">Se mueve.</p>
<p class="text-center">Se mueve el Circoled.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Se mueve.</p>
<p class="text-center">Te pudre.</p>
<p class="text-center">Te mueres.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Te mueres, de verdad, es muerte,</p>
<p class="text-center">Te pudres, te mueves, te mueres.</p>
<p class="text-center">Eres de Izquerda.</p>
<br>
<p class="text-center">Afuera abusan de gente y yo bailaré.</p>
<p class="text-center">Olvido esa gente.</p>
<br>
<p class="text-center">Todo es brillante y afuera decadente.</p>
<p class="text-center">Olvido esa gente.</p>
<br>
<p class="text-center">Se mueve.</p>
<p class="text-center">Se mueve Circoled.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Se mueve.</p>
<p class="text-center">Te pudre.</p>
<p class="text-center">Te mueres.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Se mueve.</p>
<p class="text-center">Se pudre Circoled.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Eso fue.</p>
<p class="text-center">Nos pudre.</p>
<p class="text-center">Te mueres.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Eso fue.</p>
<p class="text-center">Se muere Circoled.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Y Corroe.</p>
<p class="text-center">Te pudre.</p>
<p class="text-center">Te mueres.</p>
<p class="text-center">En el Circoled.</p>
<br>
<p class="text-center">Los mueres, de verdad, la muerte,</p> 
<p class="text-center">lo pudres, te mueves, y mueres.</p>
<p class="text-center">Eres de Derecha.</p>