---
layout: post
title:  "Heterocromia"
date:   2024-11-01 18:30:35 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Heterocromía.</p>
<p class="text-center">Heterocromía.</p>
<p class="text-center">Se que algún día no estarás.</p>
<p class="text-center">Y quedaré atrás.</p>
<br>
<p class="text-center">Mientras mueves tu colita.</p>
<p class="text-center">Esa que siempre pones en c .</p>
<p class="text-center">Te comportas en desorden.</p>
<p class="text-center">Porque hueles a frituras.</p>
<br>
<p class="text-center">Mi peludo fiel.</p>
<p class="text-center">Cuando tus tontería hacen mi día.</p>
<p class="text-center">Mi pulgoso fiel.</p>
<p class="text-center">Cambiarte, sería total villanía.</p>
<br>
<p class="text-center">Otro juguete a comprar.</p>
<br>
<p class="text-center">Heterocromía.</p>
<p class="text-center">Heterocromía.</p>
<p class="text-center">A más de un metro no te vas.</p>
<p class="text-center">Pero quedó atrás.</p>
<br>
<p class="text-center">Pones la cara de inocencia.</p>
<p class="text-center">Cuando rompes tus juguetes.</p>
<p class="text-center">Ladras a todo al que pase.</p>
<p class="text-center">Porque hueles a frituras.</p>
<br>
<p class="text-center">Tan dulce como una mañana.</p>
<p class="text-center">O un pastel de moka café.</p>
<p class="text-center">El jardín completo rompes.</p>
<p class="text-center">Luego miras como si nada.</p>
<br>
<p class="text-center">heterocromía.</p>
<p class="text-center">Heterocromía.</p>
<p class="text-center">En el jardín regalos dejaras.</p>
<p class="text-center">Y quedaré atrás.</p>
<br>
<p class="text-center">Enamorado de la primavera.</p>
<p class="text-center">Es tu mirar tan inocente.</p>
<p class="text-center">Como duermes en desorden.</p>
<p class="text-center">Porque hueles a frituras.</p>
<br>
<p class="text-center">Mi peludo fiel.</p>
<p class="text-center">Eres un maestro de la minería.</p>
<p class="text-center">Mi pulgoso fiel.</p>
<p class="text-center">Otro excavación hay que tapar.</p>
<br>
<p class="text-center">Otro juguete a comprar.</p>