---
layout: post
title:  "Al amor maduro"
date:   2024-11-01 18:30:43 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Y tu risa todo lo iluminó.</p> 
<p class="text-center">Tu pena hace todo pedazos.</p>
<p class="text-center">Tu eres el alma que quiero.</p>
<p class="text-center">Tu alma se volvió espejo.</p>
<br>
<p class="text-center">Abrazados en día nublados.</p>
<p class="text-center">Cerca cuando es mucho sol.</p>
<p class="text-center">Tu eres el alma que quiero.</p>
<p class="text-center">Mi mundo se volvió solo tu.</p>
<br>
<p class="text-center">Soy tan feliz, siendo tuyo.</p>
<p class="text-center">Junto a ti, ahora nosotros.</p>
<p class="text-center">Soy tan feliz, siendo tuyo.</p>
<br>
<p class="text-center">Nuestra vida en el mundo.</p>
<p class="text-center">El misterio sabe mi amor.</p>
<p class="text-center">Tu eres el alma que llevo.</p>
<p class="text-center">No se si en parte soy tuyo.</p>
<br>
<p class="text-center">Abrazados en días desnudos.</p>
<p class="text-center">Cerca cuando es mucho amor.</p>
<p class="text-center">Tu eres el alma que llevo.</p>
<p class="text-center">No se si en parte soy tuyo.</p>
<br>
<p class="text-center">Soy tan feliz, siendo tuyo.</p>
<p class="text-center">Junto a ti, ahora nosotros.</p>
<p class="text-center">Soy tan feliz, siendo tuyo.</p>