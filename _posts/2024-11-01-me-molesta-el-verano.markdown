---
layout: post
title:  "Me molesta el verano"
date:   2024-11-01 18:30:34 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Me molesta el verano.</p>
<p class="text-center">Disfruto de playas.</p>
<p class="text-center">Me molesta el verano.</p>
<p class="text-center">Metido en la arena.</p>
<p class="text-center">Me molesta el verano.</p>
<br>
<p class="text-center">Aunque hay helado.</p>
<p class="text-center">Y refrescos granizados.</p>
<p class="text-center">El agua es fresca.</p>
<p class="text-center">Los zapatos con arena.</p>
<br>
<p class="text-center">Me molesta el verano.</p>
<p class="text-center">El día se arranca.</p>
<p class="text-center">Me molesta el verano.</p>
<p class="text-center">Solo hay poleras.</p>
<p class="text-center">Me molesta el verano.</p>
<br>
<p class="text-center">Aún en año nuevo.</p>
<p class="text-center">Y don viejo pascuero.</p>
<p class="text-center">Se abren piscinas.</p>
<p class="text-center">Y las piezas hornea.</p>
<br>
<p class="text-center">Me molesta el verano.</p>
<p class="text-center">El invierno ya.</p> 
<p class="text-center">Me molesta el verano.</p>
<p class="text-center">La lluvia ahora.</p>
<p class="text-center">Me molesta el verano.</p>
<p class="text-center">Se acabó.</p>