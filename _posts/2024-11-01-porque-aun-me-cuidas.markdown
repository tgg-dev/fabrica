---
layout: post
title:  "¿Porque aun me cuidas?"
date:   2024-11-01 18:30:44 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Y amiga me gustabas.</p>
<p class="text-center">Creo no pensar mal.</p>
<p class="text-center">Nosotros el existir.</p>
<p class="text-center">Y empezamos a vivir.</p>
<br>
<p class="text-center">Y tu mano joven y tersa.</p>
<p class="text-center">Mi reflejo quizá.</p> 
<p class="text-center">Este en tu alma.</p>
<br>
<p class="text-center">Todo lo mio sera tuyo,</p> 
<p class="text-center">Porque aun me cuidas?</p>
<br>
<p class="text-center">En soledad caminaba.</p>
<p class="text-center">O tal vez imaginaba.</p>
<p class="text-center">Llego todo el color.</p>
<p class="text-center">Ella trajo el color.</p>
<br>
<p class="text-center">Y tu mano bella y tersa.</p>
<p class="text-center">Mi reflejo quizá.</p>
<p class="text-center">Esta en tu alma.</p>
<br>
<p class="text-center">Todo lo mío es tuyo,</p> 
<p class="text-center">Porque aun me cuidas?</p>
<br>
<p class="text-center">Pelo blanco nos saldrá.</p>
<p class="text-center">Entenderemos todo más.</p>
<p class="text-center">Tomaremos esas onces.</p>
<p class="text-center">Y veremos esas series.</p>
<br>
<p class="text-center">Y aun eres joven y tersa.</p>
<p class="text-center">Mi reflejo quizá.</p> 
<p class="text-center">Esta en tu alma.</p>
<br>
<p class="text-center">Lo mio es siempre tuyo,</p> 
<p class="text-center">Porque aun me cuidas?</p>
<br>
<p class="text-center">Ya vendrá ese atardecer.</p>
<p class="text-center">El siempre va a vencer.</p>
<p class="text-center">La memoria va a fallar.</p>
<p class="text-center">El siempre te voy amar.</p>
<br>
<p class="text-center">Y aun eres bella y tersa.</p>
<p class="text-center">Mi reflejo quizá.</p> 
<p class="text-center">Esta en tu alma.</p>
<br>
<p class="text-center">Lo mio siempre sera tuyo,</p> 
<p class="text-center">Porque aun me cuidas?</p>
<p class="text-center">No lo merezco.</p>
<br>
<p class="text-center">Porque aun me cuidas?</p>
<p class="text-center">No es lo correcto.</p>
<br>
<p class="text-center">Porque aun me cuidas?</p>