---
layout: post
title:  "Frente a ti (al imposible amor)"
date:   2024-11-01 18:30:40 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Contigo no hay oscuridad.</p>
<p class="text-center">Esa luz que ya me atrapa.</p>
<p class="text-center">Fresca igual a la lluvia.</p>
<p class="text-center">Aroma a pasto en mañana.</p>
<br>
<p class="text-center">Soñe en dormir abrazados.</p>
<p class="text-center">En un bosque color azul.</p>
<p class="text-center">Flores blancas tornasol.</p>
<p class="text-center">Sonó de fondo la canción.</p>
<br>
<p class="text-center">Frente a ti.</p>
<p class="text-center">Ya soy una verdad.</p>
<p class="text-center">Frente a ti.</p>
<p class="text-center">Preso en libertad.</p>
<p class="text-center">Frente a ti.</p>
<p class="text-center">No veo la maldad.</p>
<p class="text-center">Frente a ti.</p>
<p class="text-center">Ya olvidó lo demás.</p>
<br>
<p class="text-center">Ahora tiempo no paras.</p>
<p class="text-center">Esta casa es tu hogar.</p>
<p class="text-center">Contigo veo las horas.</p>
<p class="text-center">Mi zapatos ya no están.</p>
<br>
<p class="text-center">Los recuerdos del ayer.</p>
<p class="text-center">Amarte que solo padecer.</p>
<p class="text-center">Aprendimos juntos a ser.</p>
<p class="text-center">Lo que en soledad no fue.</p>
<br>
<p class="text-center">Frente a ti.</p>
<p class="text-center">El destino vendrá.</p>
<p class="text-center">Frente a ti.</p>
<p class="text-center">Quiero morir ya.</p>
<p class="text-center">Frente a ti.</p>
<p class="text-center">No se que pasara.</p>
<p class="text-center">Frente a ti.</p>
<p class="text-center">Ya olvidó lo demás.</p>
<br>
<p class="text-center">Era algo mayor.</p>
<p class="text-center">Era algo menor.</p>
<p class="text-center">Y olía el amor.</p>
<p class="text-center">Sentía el amor.</p>
<br>
<p class="text-center">Eso era hoy o era ayer.</p>
<p class="text-center">No preguntes ya no se.</p>
<p class="text-center">El dolor enseña bien.</p>
<p class="text-center">Nada dura por siempre.</p>
<br>
<p class="text-center">Ese sol ya no estará.</p>
<p class="text-center">Tu sonreír no está más.</p>
<p class="text-center">Esas condenas volverán.</p>
<p class="text-center">Mi zapatos ya no están.</p>
<br>
<p class="text-center">Y sin ti.</p>
<p class="text-center">Eso jamás iba volar.</p>
<p class="text-center">Y sin ti.</p>
<p class="text-center">Ya no tengo libertad.</p>
<p class="text-center">Y sin ti.</p>
<p class="text-center">Solo veo esa maldad.</p>
<br>
<p class="text-center">Y sin ti.</p>
<p class="text-center">Ya vuelven lo demás.</p>
<br>
<p class="text-center">Soñé en dormir abrazados.</p>
<p class="text-center">En un bosque color azul.</p>
<p class="text-center">Flores blancas tornasol.</p>
<p class="text-center">Desperté siendo cortado.</p>
<br>
<p class="text-center">Contigo no hay la mañana.</p>
<p class="text-center">Esa luz que ya me quema.</p>
<p class="text-center">Interior fue ese arenal.</p>
<p class="text-center">El pasado es oscuridad.</p>