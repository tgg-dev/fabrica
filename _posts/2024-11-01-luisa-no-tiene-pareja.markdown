---
layout: post
title:  "Luisa no tiene pareja"
date:   2024-11-01 18:30:24 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Luisa nació fuera de la Tierra.</p>
<p class="text-center">Cuando en Marte hacían viviendas.</p>
<br>
<p class="text-center">La tragedia del ser .</p>
<p class="text-center">O vivir en marte.</p>
<p class="text-center">Luisa no tiene pareja.</p>
<br>
<p class="text-center">Luisa está viviendo con Fernanda.</p>
<p class="text-center">Compran una casa en la Tierra.</p>
<br>
<p class="text-center">Pero se harta del ser.</p>
<p class="text-center">Y es mejor parecer.</p>
<p class="text-center">Luisa no tiene pareja.</p>
<br>
<p class="text-center">Ahora ya no es una joven y loca como ayer.</p>
<p class="text-center">La adulta ya se controla y no es loca como fue.</p>
<p class="text-center">Ahora ella cree ser bella y pura al atardecer.</p>
<br>
<p class="text-center">Luisa en un asilo de vejez.</p>
<p class="text-center">Esconde lo que ella fue.</p>
<br>
<p class="text-center">No hubo raros amores.</p>
<p class="text-center">Ella es tan pura.</p>
<p class="text-center">Luisa no tuvo pareja.</p>
<br>
<p class="text-center">Luisa en una nave viaja sola.</p>
<p class="text-center">En ataúd a Marte sin demora.</p>
<br>
<p class="text-center">Y al rojo atardecer.</p>
<p class="text-center">La entierran bien.</p>
<p class="text-center">Luisa no tenía pareja.</p>
