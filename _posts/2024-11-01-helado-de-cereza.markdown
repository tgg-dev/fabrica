---
layout: post
title:  "Helado de cereza"
date:   2024-11-01 18:30:22 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">No tenia como aprender.</p>
<p class="text-center">Ella me hace temblar.</p>
<p class="text-center">Puso su dedo en mi ser.</p>
<p class="text-center">Quería algo comentar.</p>

<p class="text-center">Luego vino su desnudez.</p>
<p class="text-center">Me comenzó a callar.</p>
<p class="text-center">Fríos ojos me envuelve.</p>
<p class="text-center">la sonrisa perfecta.</p>

<p class="text-center">No puedo sacar de mi mente su boca.</p>
<p class="text-center">Movimientos felinos que atormentan.</p>
<p class="text-center">Tenia la boca como una cereza.</p>

<p class="text-center">Extraño ese invierno.</p>
<p class="text-center">Ojos profundos y azules.</p>
<p class="text-center">Mientras hacia frio.</p>
<p class="text-center">Los cuerpos eran Lumbre.</p>
<p class="text-center">Feliz muero en esto.</p>

<p class="text-center">No puedo sacar de mi mente su boca.</p>
<p class="text-center">Movimientos felinos que atormentan.</p>
<p class="text-center">Tenia la boca como una cereza.</p>
