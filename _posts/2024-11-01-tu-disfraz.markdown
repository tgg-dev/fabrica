---
layout: post
title:  "Tu Disfraz"
date:   2024-11-01 18:30:19 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Ya sé lo que no es evidente a todos.</p>
<p class="text-center">Incluso a también a ti, como a todos.</p>
<p class="text-center">Si no quieres llorar.</p>
<p class="text-center">No veas el final.</p>
<br>
<p class="text-center">Usa tu disfraz y se igual.</p>
<p class="text-center">Tu mama y tu papa ya lo usan.</p>
<p class="text-center">Usa tu disfraz y se un ser ejemplar.</p>
<br>
<p class="text-center">Si te dijera que te conozco a diario.</p>
<p class="text-center">Piensas que tu no eres del cotidiano.</p>
<p class="text-center">Vistes diferente a aquellos macacos.</p>
<p class="text-center">Y cuatro extremidades te delataron.</p>
<br>
<p class="text-center">Ya se lo que no es evidente a todos.</p>
<p class="text-center">Incluso a también a ti, como a todos.</p>
<p class="text-center">Si no quieres llorar
<p class="text-center">No vayas a flaquear
<br>
<p class="text-center">Usa tu disfraz y se igual.</p>
<p class="text-center">Tu mama y tu papa ya lo usan.</p>
<p class="text-center">Usa tu disfraz y se un ser ejemplar.</p>
<br>
<p class="text-center">Siendo punk, mod, rocker o un flaite .</p>
<p class="text-center">Todas esas banderas y no te escondes?</p>
<p class="text-center">Ahora viste fuera de tu soñado orden.</p>
<p class="text-center">Dejaron en tu piel esas viejas voces.</p>
<br>
<p class="text-center">Ya sabes, y no es evidente a todos.</p>
<p class="text-center">Incluso a también a ti, como a todos.</p>
<p class="text-center">Ahora no vayas a llorar.</p>
<p class="text-center">Eres curioso al final.</p>
<br>
<p class="text-center">Usa tu disfraz ya eres igual.</p>
<p class="text-center">Eres tu mama o tu papá y no se escapa.</p>
<p class="text-center">Tu disfraz, el derecho a ser ese número más…</p>