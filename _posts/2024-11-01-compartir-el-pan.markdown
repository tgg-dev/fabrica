---
layout: post
title:  "Intrexto"
date:   2024-11-01 18:30:50 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Compartir el pan.</p>
<p class="text-center">Un mundo perfecto.</p>
<p class="text-center">Siempre se escapara.</p>
<p class="text-center">Siendo imperfectos.</p>
<p class="text-center">Esa es tu hermosura.</p>
<br>
<p class="text-center">Triunfar.</p> 
<p class="text-center">No es mirar abajo a tu prójimo.</p>
<br>
<p class="text-center">Equidad e igualdad.</p>
<p class="text-center">Eso es tan imperfecto.</p>
<p class="text-center">Ahí está la belleza.</p>
<p class="text-center">Para crear todo esto.</p>
<br>
<p class="text-center">No germinara,</p> 
<p class="text-center">olvidando todo, a tu prójimo.</p>
<br>
<p class="text-center">Comparte tu pan.</p>
<p class="text-center">Comparte tu pan.</p>
<br>
<p class="text-center">Tu esperanza.</p> 
<p class="text-center">Es siempre mirando a tu prójimo.</p>
<br>
<p class="text-center">Una vez se arranca.</p>
<p class="text-center">Es confuso imperfecto.</p>
<p class="text-center">Unos son más igual.</p>
<p class="text-center">Debería bastarte eso.</p>
<br>
<p class="text-center">Nunca avanzaras,</p> 
<p class="text-center">Robando o matando a tu prójimo.</p>
<br>
<p class="text-center">El no fallaría.</p>
<p class="text-center">En su sacrificio.</p>
<p class="text-center">Pero la codicia.</p>
<p class="text-center">Es del ser humano.</p>
<br>
<p class="text-center">Triunfar,</p> 
<p class="text-center">No es olvidarlo todo a tu prójimo.</p>
<br>
<p class="text-center">Comparte tu pan.</p>
<p class="text-center">Comparte tu pan.</p>
<br>
<p class="text-center">Olvidar,</p> 
<p class="text-center">Será renunciar por todo a tu prójimo.</p>
<br>
<p class="text-center">Se siente como todo va a cambiar.</p>
<p class="text-center">Ahora entendemos que el pasado enseñara.</p>
<p class="text-center">Esa nueva era se llegara ahora.</p>
<br>
<p class="text-center">Comparte tu pan.</p>
<p class="text-center">Comparte tu pan.</p>
<br>
<p class="text-center">Amanecer perfecto.</p>
<p class="text-center">No se puede alcanzar.</p>
<p class="text-center">Nunca es correcto.</p>
<p class="text-center">A tu alma traicionar.</p>
<br>
<p class="text-center">El punto del ocaso,</p> 
<p class="text-center">suéltalo todo a tu prójimo.</p>
<p class="text-center">Tu y el prójimo Son los reflejos.</p>
<p class="text-center">De los humanos.</p>
<p class="text-center">Comparte tu pan.</p>
<p class="text-center">Comparte tu pan.</p>