---
layout: post
title:  "Tocar el pasto"
date:   2024-11-01 18:30:37 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">El limón ya floreció.</p>
<p class="text-center">El durazno blanqueo.</p>
<p class="text-center">La abejas y abejorros.</p>
<p class="text-center">Ahora abunda ese sol.</p>
<p class="text-center">La primavera ya llegó.</p>
<br>
<p class="text-center">Porque no hay nada más elegante.</p>
<p class="text-center">Que tocar el pasto.</p>
<br>
<p class="text-center">No es frío ni calor.</p>
<p class="text-center">Este clima es mejor.</p>
<p class="text-center">Es invierno y verano.</p>
<p class="text-center">Manga corta nublado.</p>
<p class="text-center">Y enanos alérgicos.</p>
<p class="text-center">Es lindo y peluson.</p>
<br>
<p class="text-center">Porque no hay nada más elegante.</p>
<p class="text-center">Que tocar el pasto.</p>
<br>
<p class="text-center">No importa que vendrá.</p>
<p class="text-center">El viejo pascuero.</p>
<p class="text-center">En guayabera.</p>
<br>
<p class="text-center">Tu sabes lo que pasara.</p>
<p class="text-center">El estado entero.</p>
<p class="text-center">Florecerá.</p>
<br>
<p class="text-center">Porque no hay nada más elegante.</p>
<p class="text-center">Que tocar el pasto.</p>
<br>
<p class="text-center">l organillo vendrá.</p>
<p class="text-center">Limoncello tendrás.</p>
<p class="text-center">En guayabera.</p>
<br>
<p class="text-center">Antialérgico a comprar.</p>
<p class="text-center">El estado entero.</p>
<p class="text-center">Renacerá.</p>
<br>
<p class="text-center">El lago no esta frio.</p>
<p class="text-center">Y el cono de helado.</p>
<p class="text-center">Y a ducharse seguido.</p>
<p class="text-center">Perfumes de a litro.</p>
<p class="text-center">Atardecer muy fresco.</p>
<p class="text-center">Ahora el verano llegó.</p>
<br>
<p class="text-center">Porque no hay nada más elegante.</p>
<p class="text-center">Que tocar el pasto.</p>
<br>
<p class="text-center">Dije que no hay nada más elegante.</p>
<p class="text-center">Que tocar el pasto.</p>