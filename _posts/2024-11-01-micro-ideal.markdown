---
layout: post
title:  "Micro Ideal"
date:   2024-11-01 18:30:27 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Asiento tras asiento.</p>
<p class="text-center">Hombros tras hombros.</p>
<br>
<p class="text-center">Mañana, cara de esperpento.</p>
<p class="text-center">En una lata con ruedas.</p>
<p class="text-center">El amanecer sumamente lento.</p>
<p class="text-center">Todos miramos la vereda.</p>
<br>
<p class="text-center">Sumando las horas de tacos.</p>
<p class="text-center">Dañando el ano tendrás.</p>
<p class="text-center">Ese chofer manejando duro.</p>
<p class="text-center">Se lleva tu eternidad.</p>
<br>
<p class="text-center">Viene, Viene, Viene.</p> 
<p class="text-center">Es como soñar.</p>
<p class="text-center">Viene, Viene, Viene.</p> 
<p class="text-center">La Micro ideal.</p> 
<br>
<p class="text-center">Persona a persona.</p>
<p class="text-center">Miradas a Miradas.</p>
<br>
<p class="text-center">En la tarde, olor a sudor.</p>
<p class="text-center">Hartos miramos la nada.</p>
<p class="text-center">La noche pasara muy veloz.</p>
<p class="text-center">En una lata con ruedas.</p> 
<br>
<p class="text-center">Sumando las horas de tacos.</p>
<p class="text-center">Dañando ahora rodillas.</p>
<p class="text-center">Ese chofer manejando duro.</p>
<p class="text-center">Volviendo a esas camas.</p>
<br>
<p class="text-center">Viene, Viene, Viene.</p>
<p class="text-center">La Micro ideal.</p> 
<p class="text-center">Viene, Viene, Viene.</p> 
<p class="text-center">La Micro ideal.</p>
<p class="text-center">Viene, Viene, Viene.</p> 
<p class="text-center">La Micro ideal.</p> 
<p class="text-center">Viene, Viene, Viene.</p> 
<p class="text-center">Ahora a soñar.</p>