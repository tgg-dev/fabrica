---
layout: post
title:  "La nación Anestesia"
date:   2024-11-01 18:30:31 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Donde.</p>
<p class="text-center">Donde has estado.</p>
<p class="text-center">Yo he estado acá .</p>
<p class="text-center">Viendo como esto se vuelve una catedral.</p>
<br>
<p class="text-center">O lo que fue.</p>
<p class="text-center">O tal ves no fue.</p>
<p class="text-center">Mientras pienso tropiezo.</p>
<br>
<p class="text-center">Ves.</p>
<p class="text-center">Ves como lo entregaron.</p>
<p class="text-center">Ya nadie es nadie y todos son pocos en el lugar.</p>
<br>
<p class="text-center">Es como es.</p>
<p class="text-center">Y no puede ser.</p>
<p class="text-center">Se detuvo todo.</p>
<br>
<p class="text-center">Estar solo, da igual.</p>
<p class="text-center">Puedes pagar por fotos planas.</p>
<p class="text-center">Sentir o amar.</p>
<p class="text-center">Ahora mejor lo entiende una IA.</p>
<br>
<p class="text-center">Hay publicidad, que te ilumina el rostro.</p>
<p class="text-center">Hay un hogar, poblaciones tipo sarcófago.</p>


<p class="text-center">Ven a la nación anestesia.</p>
<p class="text-center">El mejor lugar, para programar.</p>
<p class="text-center">Ven a la nación anestesia.</p>
<p class="text-center">el autismo no se notara jamás.</p>

<p class="text-center">Reflejo.</p>
<p class="text-center">Reflejo de ambos.</p>
<p class="text-center">Nadie puede ir a Marte.</p>
<p class="text-center">A pensar ser libre.</p>

<p class="text-center">Pero las cadenas de ADN.</p>
<p class="text-center">Son cadenas al final.</p>
<p class="text-center">Mientras piensas tropezar.</p>

<p class="text-center">Estar solo, da igual.</p>
<p class="text-center">Puedes pagar por fotos planas.</p>
<p class="text-center">Sentir o amar.</p>
<p class="text-center">Ahora mejor lo entiende una IA.</p>

<p class="text-center">Buscas amistad, Dentro de una aplicación.</p>
<p class="text-center">Nadie te rechaza, solo das me gusta o no .</p>

<p class="text-center">Ven a la nación anestesia.</p>
<p class="text-center">Ser superficial, para no amar.</p>
<p class="text-center">Ven a la nación anestesia.</p>
<p class="text-center">Ser lgtbiq o mas no te salvara.</p>
<p class="text-center">Ven a la nación anestesia.</p>
<p class="text-center">Busco en tu mirar, algo falta.</p>
<p class="text-center">Ven a la nación anestesia.</p>
<p class="text-center">Inversionistas, producen más.</p>