---
layout: post
title:  "Lee tu alma"
date:   2024-11-01 18:30:47 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Ellos piensan mal.</p>
<p class="text-center">No todo es la cara larga.</p>
<p class="text-center">Mira esa ventana.</p>
<p class="text-center">Y te ves como una vieja.</p>
<br>
<p class="text-center">Piensas cómo actuar.</p>
<p class="text-center">Pero ni sabes como arreglar.</p>
<p class="text-center">Sacude lo mental.</p>
<p class="text-center">O tu vida va a una subasta.</p>
<br>
<p class="text-center">Lee tu alma.</p>
<p class="text-center">Lee tu alma.</p>
<p class="text-center">Lee tu amar.</p>
<br>
<p class="text-center">Quedarse tranquilo.</p>
<p class="text-center">No significa ser muñeco.</p>
<p class="text-center">Ideario es sano.</p>
<p class="text-center">Lee aunque sea el boleto.</p>
<br>
<p class="text-center">Que el sol entre.</p>
<p class="text-center">Ideas que solo te liberen.</p>
<p class="text-center">Pero el día déjale.</p>
<p class="text-center">Tus hijos quedan después.</p>
<br>
<p class="text-center">Piensas cómo actuar.</p>
<p class="text-center">No todo es la cara larga.</p>
<p class="text-center">Mira esa ventana.</p>
<p class="text-center">O tu vida va a una subasta.</p>
<br>
<p class="text-center">Lee tu alma.</p>
<p class="text-center">Lee tu alma.</p>
<p class="text-center">Lee tu alma.</p>
<p class="text-center">Lee tu alma.</p>
<p class="text-center">Lee tu amar.</p>