---
layout: post
title:  "Intrexto"
date:   2024-11-01 18:30:49 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">En Cristal
<p class="text-center">Los momentos que vivimos.</p>
<p class="text-center">Son como hojas de árboles.</p>
<p class="text-center">Son imagen y el contexto.</p>
<p class="text-center">Adjetivo de el desaparecer.</p>
<br>
<p class="text-center">Como si Descartes leyera a Maquiavelo.</p>
<p class="text-center">Como si Voltaire hiciera unos negocios.</p>
<p class="text-center">La ironía que todo estará envuelto.</p> 
<p class="text-center">En cristal delicado.</p>
<br>
<p class="text-center">El frio y pájaros coquetos.</p>
<p class="text-center">Amor platónico y nunca fue.</p>
<p class="text-center">Eso recuerdos son diluidos.</p>
<p class="text-center">Sinónimo de ser tan leves.</p>
<br>
<p class="text-center">Como si Descartes leyera a Maquiavelo.</p>
<p class="text-center">Como si Voltaire hiciera unos negocios.</p>
<p class="text-center">Gracioso lo inocuo de ese recuerdo.</p>
<p class="text-center">En cristal delicado.</p>
<br>
<p class="text-center">Como si Descartes leyera a Maquiavelo.</p>
<p class="text-center">Como si Voltaire hiciera unos negocios.</p>
<p class="text-center">La ironía que todo eso es recuerdos.</p>
<p class="text-center">En cristal delicado.</p>
<p class="text-center">Es cristal atesorado.</p>