---
layout: post
title:  "Cuento de tres revoltosos"
date:   2024-11-01 18:30:36 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">El Kargar Pulgoso era su bar,</p>
<p class="text-center">La guardia del reino voló el lugar.</p>
<p class="text-center">Rosita había golpeado, a una patrulla.</p>
<p class="text-center">Los osos de Erdus escapan de Sur Amazonía.</p>
<br>
<p class="text-center">Rhosita y Khasioso.</p>
<p class="text-center">Cruzaron toda Luxcan,</p>
<p class="text-center">No importa cómo,</p>
<p class="text-center">El bar debía vengarse.</p>
<br>
<p class="text-center">Se encontraron con Piel de Luna,</p>
<p class="text-center">Lo acompañaron de vuelta a Sur Amazonía.</p>
<p class="text-center">Rhosita, las armas, Khasioso comida y cucharas,</p>
<p class="text-center">Porque las armas no las sabe usar.</p>
<br>
<p class="text-center">Rhosita y Khasioso.</p>
<p class="text-center">Cruzaron toda Luxcan,</p>
<p class="text-center">No importa dónde,</p>
<p class="text-center">Solo querían venganza.</p>
<br>
<p class="text-center">Los tres con pulgas en Khalon,</p>
<p class="text-center">Rascándose en la ribera de Toltol.</p>
<p class="text-center">Piel de luna la acción, Rhosita la emoción,</p>
<p class="text-center">Khasioso comiendo, palo limón.</p>
<br>
<p class="text-center">Rhosita y Khasioso.</p>
<p class="text-center">cruzaron toda Luxcan,</p>
<p class="text-center">No importa dónde,</p>
<p class="text-center">Ya querían venganza.</p>
<br>
<p class="text-center">Sube y sube van Norte Amazonía,</p>
<p class="text-center">En Erdus ya son héroes local,</p>
<p class="text-center">los corsarios del reino tiemblan.</p>
<p class="text-center">El Emperador ya no los quiere,</p>
<p class="text-center">Donde van causan revoluciones,</p>
<p class="text-center">En arabesco por fin los encierran.</p>
<br>
<p class="text-center">Una vez en Arabesco,</p>
<p class="text-center">La cárcel se llamada Libertad.</p>
<p class="text-center">Liberaron, pensadores,</p>
<p class="text-center">Profesores y doctores,</p>
<p class="text-center">Revolución en toda la ciudad.</p>
<br>
<p class="text-center">Rhosita y Khasioso.</p>
<p class="text-center">cruzaron toda Luxcan,</p>
<p class="text-center">No importa dónde,
<p class="text-center">Ahora querían salir de ahí.</p>
<br>
<p class="text-center">Llegando a Norte Amazonía,</p>
<p class="text-center">Se siente el olor a libertad.</p>
<p class="text-center">Los osos no saben nadar,</p>
<p class="text-center">La nave en medio del mar,</p>
<p class="text-center">Piel de Luna lo lleva a rastras.</p>
<br>
<p class="text-center">Rhosita y Khasioso,</p>
<p class="text-center">Luxcan no los olvidará,</p>
<p class="text-center">No importa dónde,</p>
<p class="text-center">Dejaron revoluciones ahí.</p>
<br>
<p class="text-center">No importa dónde,</p>
<p class="text-center">A Erdus se van por fin.</p>