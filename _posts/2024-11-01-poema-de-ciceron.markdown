---
layout: post
title:  "Poema de Cicerón"
date:   2024-11-01 18:30:38 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Estaba tan dispuesto.</p>
<p class="text-center">Un señor elegante.</p>
<p class="text-center">En su jaulilla jugaba.</p>
<p class="text-center">Sin molestarme.</p>
<br>
<p class="text-center">Cicerón,</p> 
<p class="text-center">Era un gran señor.</p>
<p class="text-center">Cicerón,</p> 
<p class="text-center">Y su traje blanco.</p>
<br>
<p class="text-center">Estricto vegetariano.</p>
<p class="text-center">Evitaba carnes.</p>
<p class="text-center">Escuchaba la sonata.</p>
<p class="text-center">Leía a Voltaire.</p>
<br>
<p class="text-center">Cicerón,</p> 
<p class="text-center">Era un gran señor.</p>
<p class="text-center">Cicerón,</p> 
<p class="text-center">Fue hermoso ratón.</p>
<br>
<p class="text-center">Un día quiso ir al parque.</p>
<p class="text-center">Salir, liberarse.</p>
<p class="text-center">Uno comete errores fatales.</p>
<p class="text-center">Saliendo al parque.</p>
<br>
<p class="text-center">Cicerón,</p> 
<p class="text-center">Era mi primer ratón.</p>
<p class="text-center">Cicerón,</p> 
<p class="text-center">Mi hermoso juguetón.</p>
<br>
<p class="text-center">El cielo ratonesco.</p>
<p class="text-center">Lleno de salones.</p>
<p class="text-center">Castores de vecinos.</p>
<p class="text-center">Leyendo a Voltaire.</p>
<br>
<p class="text-center">Cicerón,</p> 
<p class="text-center">Hermoso te extraño.</p>
<p class="text-center">Cicerón,</p> 
<p class="text-center">Entierro de libro.</p>