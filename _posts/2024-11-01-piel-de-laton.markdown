---
layout: post
title:  "Piel de latón"
date:   2024-11-01 18:30:51 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Si es o no es, tal vez fue.</p>
<p class="text-center">Camino ese sendero.</p>
<br>
<p class="text-center">Enviado a esos pescadores.</p>
<p class="text-center">Mueblista y ovejero.</p>
<br>
<p class="text-center">De latón era toda su piel.</p>
<p class="text-center">Pero lo blanquearon.</p>
<br>
<p class="text-center">Hijo de la más hermosa madre.</p>
<p class="text-center">Camino ese sendero por ellos.</p>
<br>
<p class="text-center">Todos lo esperaban siempre.</p>
<p class="text-center">Para que haya vino.</p>
<br>
<p class="text-center">Fuiste a lejanos lugares.</p>
<p class="text-center">Pero te blanquearon.</p>
<br>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Tu madre espera.</p>
<p class="text-center">Afuera.</p>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Piel de laton.</p>
<br>
<p class="text-center">Con parábolas y juegos sale.</p>
<p class="text-center">Y va a un cerro como ovejero.</p>
<br>
<p class="text-center">Fuiste a lejanos lugares.</p>
<p class="text-center">Pero lo olvidaron.</p>
<br>
<p class="text-center">Hermano ya no hay peces.</p>
<p class="text-center">Tu amigos fallaron.</p>
<br>
<p class="text-center">Mama yo quiero abrazarte.</p>
<p class="text-center">Pero mi fin es alto.</p>
<br>
<p class="text-center">Hijo de la mas hermosa madre.</p>
<p class="text-center">Camino ese sendero por ellos.</p>
<p class="text-center">Hará algo bello y va renacer.</p>
<br>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Tu madre espera.</p>
<p class="text-center">Afuera.</p>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Piel de laton.</p>

<p class="text-center">Parábolas y no juegos vuelven.</p>
<p class="text-center">Ese monte, el nosotros cambio.</p>

<p class="text-center">Hijo de la mas hermosa madre.</p>
<p class="text-center">Ese monte,lo volvió el pastor.</p>

<p class="text-center">Porque Hermano no había peces.</p>

<p class="text-center">Piel de laton.</p>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Tu madre espera.</p>
<p class="text-center">Afuera.</p>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Piel de laton.</p>
<p class="text-center">Con parábolas y juegos sale.</p>
<p class="text-center">Y va a un cerro como ovejero.</p>

<p class="text-center">Pescador.</p>
<p class="text-center">Carpintero.</p>
<p class="text-center">Hijo prodigo.</p>
<p class="text-center">Y libre pensador...</p>
