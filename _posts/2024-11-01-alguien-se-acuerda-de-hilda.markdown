---
layout: post
title:  "Alguien se acuerda de Hilda"
date:   2024-11-01 18:30:25 -0300
categories: poesia
featured_image: https://www.vectorlogo.zone/logos/jekyllrb/jekyllrb-ar21.png
---
<p class="text-center">Con su pelo dorado.</p>
<p class="text-center">Su mirada de fuego.</p>
<p class="text-center">Nadie la vio partir.</p>
<br>
<p class="text-center">Cuidar hijos ajenos.</p>
<p class="text-center">El jarabe fue lento.</p>
<p class="text-center">¿Te acuerdas de mí?.</p>
<br>
<p class="text-center">Fuimos condenados.</p>
<p class="text-center">A correr de todos.</p>
<p class="text-center">Pero yo no lo creí.</p>
<p class="text-center">Y repetía otra vez.</p>
<p class="text-center">Eso de punk not death.</p>
<br>
<p class="text-center">Hilda vamos a gritar.</p>
<p class="text-center">Soltamos toda rabia.</p>
<br>
<p class="text-center">Alguien se acuerda de Hilda.</p>
<p class="text-center">De esa Hilda,</p>
<p class="text-center">La única Hilda.</p>
<br>
<p class="text-center">Alguien se acuerda de Hilda.</p>
<p class="text-center">De esa Hilda,</p>
<p class="text-center">La única Hilda.</p>
<br>
<p class="text-center">Speed bien puesto.</p>
<p class="text-center">El segundo momento.</p>
<p class="text-center">Nadie la vio venir.</p>
<br>
<p class="text-center">Locuras a momentos.</p>
<p class="text-center">Jarabe está bueno.</p>
<p class="text-center">Olvidarme de ti?.</p>
<br>
<p class="text-center">Fuimos bendecidos.</p>
<p class="text-center">A correr de todos.</p>
<p class="text-center">Pero yo no lo creí.</p>
<p class="text-center">Y repetía otra vez.</p>
<p class="text-center">Eso de punk not death.</p>
<br>
<p class="text-center">Hilda me matas ahora.</p>
<p class="text-center">Así me duermo en paz.</p>
<br>
<p class="text-center">Alguien se acuerda de Hilda.</p>
<p class="text-center">De esa Hilda,</p>
<p class="text-center">la única Hilda.</p>
<br>
<p class="text-center">Alguien se acuerda de Hilda.</p>
<p class="text-center">De esa Hilda,</p>
<p class="text-center">la única Hilda.</p>
<br>
<p class="text-center">Con su pelo dorado.</p>
<p class="text-center">Su mirada de cielo.</p>
<p class="text-center">Yo ya mas no la vi.</p>
<br>
<p class="text-center">Tuvo hijos propios.</p>
<p class="text-center">El jarabe olvidado.</p>
<p class="text-center">¿Te acuerdas de ti?</p>
<br>
<p class="text-center">Fuimos arreglados.</p>
<p class="text-center">Y cambiamos todos.</p>
<p class="text-center">Pero yo no lo creí.</p>
<p class="text-center">Y repetía otra vez.</p>
<p class="text-center">Eso de punk not death.</p>
<br>
<p class="text-center">Hilda ahora arranca.</p>
<p class="text-center">De la cerda justicia.</p>
<br>
<p class="text-center">Alguien se acuerda de Hilda.</p>
<p class="text-center">De esa Hilda,</p>
<p class="text-center">La única Hilda.</p>
<br>
<p class="text-center">Alguien se acuerda de Hilda.</p>
<p class="text-center">De esa Hilda,</p>
<p class="text-center">La única Hilda.</p>