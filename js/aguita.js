document.addEventListener('DOMContentLoaded', function() {
    const combinations = [
        { background: '#ff6600', text: '#241f1c' }, // Naranjo
        { background: '#241f1c', text: '#e3dedb' }, // Negro pasa
        { background: '#e3dedb', text: '#ff6600' }, // Gainsboro
        { background: '#FFCC00', text: '#241f1c' }, // Amarillo Mandarina
        { background: '#1f75fe', text: '#241f1c' } // Azul (Crayon)
    ];

    // Elegir una combinación aleatoria
    const randomIndex = Math.floor(Math.random() * combinations.length);
    const { background, text } = combinations[randomIndex];

    // Cambiar el color de fondo del body
    document.body.style.backgroundColor = background;

    // Cambiar el color del texto de todos los elementos dentro del body
    document.querySelectorAll('body *').forEach(element => {
        element.style.color = text;
    });

    // Cambiar el color del texto (logo y enlaces)
    const logo = document.querySelector('.logo');
    const links = document.querySelectorAll('.icono');
    logo.style.stroke = text;
    logo.style.fill = background; // Cambiar el color de relleno del logo
    document.documentElement.style.setProperty('--text', text); // Establecer el valor de la variable CSS
    logo.style.animation = 'draw 2s linear forwards, fill 2s ease-out 2s forwards'; // Ajustar la animación de relleno
    links.forEach(link => {
        link.style.color = text;
    });
});