# Fabrica v1.0.0

Fabrica es un tema para Jekyll que se basa en el tema Minima, pero con un toque misterioso y un diseño ultra minimalista. Está diseñado para mostrar tu marca y tus redes sociales de una manera destacada, priorizando la estética sobre el contenido.

## Características

- **Misterioso:** Fabrica agrega un toque de misterio a tu sitio web, creando una atmósfera única que atrae a tus visitantes.
- **Diseño ultra minimalista:** El diseño de Fabrica se enfoca en la simplicidad y la elegancia, con un enfoque especial en mostrar tu marca y tus redes sociales.
- **Personalización de colores:** Fabrica te permite jugar con los colores para adaptar el tema a tus preferencias y necesidades.

# Instalación

Para utilizar Fabrica en tu sitio de Jekyll, simplemente sigue estos pasos:

1. Clona o descarga el repositorio de Fabrica.
2. Copia los archivos del tema en la carpeta `_themes` de tu proyecto Jekyll.
3. Configura tu archivo `_config.yml` para utilizar el tema Fabrica.
4. ¡Listo! Ahora puedes empezar a personalizar tu sitio con el tema Fabrica.

## Contribución

Si encuentras algún problema o tienes alguna sugerencia para mejorar Fabrica, no dudes en [abrir un problema](https://gitlab.com/tgg-dev/fabrica/-/issues) o [enviar una solicitud de extracción](https://gitlab.com/tgg-dev/fabrica/-/merge_requests).

## Créditos

- Fabrica se basa en el tema Minima, creado por Jekyll. Agradecemos a los creadores de Minima por su excelente trabajo en el desarrollo del tema base.
- El diseño y la personalización fueron realizados por [Tomas Gajardo Gutiérrez (@TGG)](https://gitlab.com/tgg-dev).

## Licencia

Fabrica se distribuye bajo la [Licencia MIT](LICENSE), lo que significa que puedes utilizarlo libremente en tus proyectos personales o comerciales, siempre y cuando mantengas la atribución adecuada.